dataService.prototype.getBitbucketIssues = function( repository, state = 'open' ) {

  var _this = this,
      query;

  switch ( state ) {

    case 'open' :
      query = '?q=state!="resolved"';
      break;

    case 'resolved' :
      query = '?q=state="resolved"';
      break;

    default :
      query = '?q=state!="resolved"';
      break;

  }

  return _this.$http.get('https://api.bitbucket.org/2.0/repositories/shermanlibrary/' + repository + '/issues' + query)
      .then(function(response){
          if (typeof response.data === 'object') {
              return response.data;
          } else {
              return response.data;
          }

      }, function(response) {
          return _this.$q.reject(response.data);
      })


};
