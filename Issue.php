<?php
class Issue {

  public $fields;
  public $repository;
  public $team = 'shermanlibrary';

  public function __construct() {}

  /**
   * Creates the issue we post to the repository
   *
   * @param string $repository name of the repository
   * @param string $title required name of the issue
   * @param string $content optional long description, the body-content of the issue
   * @param string $kind can be: bug, enhancement, proposal, task
   * @param string $priorty can be: trivial, minor, major, critical, blocker
   * @param string $status can be: new, open, resolved, on hold, invalid, duplicate, wontfix
   *
   * @return json_encoded fields
   */
  public function createIssue( $repository, $title, $content = '', $kind = 'bug', $priority = 'trivial', $status = 'new' ) {

    $this->repository = $repository;
    $description = ( strlen( $title ) > 255 ? substr( $title, 256 ) : '' );
    $title = ( strlen( $title ) > 255 ? substr( $title, 0, 255 ) : $title );

    $this->fields = array(
      'content'  => array(
        'raw'    => $description . $content
      ),
      'kind'     => $kind,
      'priority' => $priority,
      'status'   => $status,
      'title'    => $title
    );

  }

  public function getIssue() {
    return json_encode( $this->fields );
  }

  public function postIssue() {

    $headers = array();
    $headers[] = 'Authorization: Basic BASE-64-ENCODED-USERNAME:PASSWORD';
    $headers[] = 'Content-Type: application/json';

    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, 'https://api.bitbucket.org/2.0/repositories/shermanlibrary/novacat/issues' );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_POST, count( $this->fields ) );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $this->fields ) );
    $result = curl_exec( $ch );
    if ( curl_errno( $ch ) ) { 
      echo 'Error: ' . curl_error( $ch );
    }
    curl_close( $ch );

  }

}
?>
