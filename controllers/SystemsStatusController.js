
(function(){

    'use strict';

    var SystemsStatusController = function( $attrs, dataService ) {

      var repository = $attrs.repository;
      var vm = this;


      function  getBitbucketIssues( repository ){

          dataService.getBitbucketIssues( repository ).then(function( data ){
              if( data ) {
                vm.issues = data.values;

                for ( var i = 0; i < vm.issues.length; i++ ) {
                  var issue = vm.issues[i];
                  if ( issue[ 'kind' ] == 'bug' && issue['priority'] == 'minor' && issue[ 'state'] !== 'resolved' ) {
                    vm.statusMinor = true;
                  }

                  if ( issue[ 'kind' ] == 'bug' && issue[ 'priority'] == 'major' && issue[ 'state'] !== 'resolved' ) {
                    vm.statusMajor = true;
                  }
                }

              }
          });
      }

      getBitbucketIssues( repository );

    }

    SystemsStatusController.$inject = ['$attrs', 'dataService'];
    angular.module('shermanApp').controller('SystemsStatusController', SystemsStatusController);

})();
