(function(){

    'use strict';

    var IssuesController = function( dataService, $routeParams ) {


        function  getBitbucketIssues( repository, state ){

            dataService.getBitbucketIssues( repository, state ).then(function( data ){
                if( data ) {
                    vm.issues = data.values;
                }
            });
        }


      if ( $routeParams.repository ) {

        var repository = $routeParams.repository;
        var state = ( $routeParams.state ? $routeParams.state :  'open' );
        var vm = this;
        vm.repository = repository;

        getBitbucketIssues( repository, state );

      }

      return false;

    }

    IssuesController.$inject = [ 'dataService', '$routeParams' ];
    angular.module('shermanApp').controller('IssuesController', IssuesController);

})();
